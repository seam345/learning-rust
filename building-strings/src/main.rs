
fn main() {


}

#[cfg(test)]
mod tests {
    use std::time::Instant;

    #[bench]
    #[test]
    fn using_plus_equal_no_format() {
        let now = Instant::now();

        let mut test_string = "".to_owned();
        for _ in 0..1_000_000 {
            test_string += "addition";
        }


        let elapsed = now.elapsed();
        println!("Elapsed: {:.2?}", elapsed);
        log::trace!("{test_string}");
    }
    #[bench]
    #[test]
    fn vec_with_reserve() {
        let now = Instant::now();

        let mut vec_string: Vec<u8> = vec!();
        vec_string.reserve_exact(8_000_000);
        for _ in 0..1_000_000 {
            for byte in "addition".as_bytes().iter() {
                vec_string.push(*byte);
            }
        }
        let test_string = String::from_utf8(vec_string).unwrap();

        let elapsed = now.elapsed();
        println!("Elapsed vec with reserve: {:.2?}", elapsed);
        log::trace!("{test_string}");
    }

    #[bench]
    #[test]
    fn vec() {
        let now = Instant::now();

        let mut vec_string: Vec<u8> = vec!();
        vec_string.reserve_exact(8_000_000);
        for _ in 0..1_000_000 {
            for byte in "addition".as_bytes().iter() {
                vec_string.push(*byte);
            }
        }
        let test_string = String::from_utf8(vec_string).unwrap();

        let elapsed = now.elapsed();
        println!("Elapsed vec with reserve: {:.2?}", elapsed);
        log::trace!("{test_string}");
    }
}