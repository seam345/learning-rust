let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };

in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.selectLatestNightlyWith (toolchain: toolchain.default))
    
    # Add some extra dependencies from `pkgs`
    # needed for yubikey library
#    pkg-config
#    pcsclite
  ];

  # coppied from https://github.com/NixOS/nixpkgs/commit/9631f5a7183cfdd8975e03d741a1a49f0100ef94
  # after looking at this issue https://github.com/NixOS/nixpkgs/issues/1784
  # looks like this isn't needed but would like to test on another machine first
#  postPatch = ''
#    sed -i 's,"libpcsclite\.so[^"]*","${pcsclite}/lib/libpcsclite.so",g' scd/scdaemon.c
#  '';

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}


