# String building


What is the most efficent way of building a big string in rust?

I came across this making zigbee2mqtt

I was torn between `string += &format!("addition")`  and `string = format!("{string}addition")`

from my understanding of += it should be more efficient as it expands string's buffer but = will take string copy to new buffer with additions then reasign string to the new buffer

but let's test both speed and memory usage!!!

```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut test_string = "".to_owned();
    for _ in 0..50_000 {
        test_string += &format!("addition");
    }

    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```
sub second


```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut test_string = "".to_owned();
    for _ in 0..50_000 {
        test_string = format!("{test_string}addition");
    }

    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```
multiple seconds


```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut test_string = "".to_owned();
    for _ in 0..1_000_000 {
        test_string += "addition";
    }

    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```

50.62ms

```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut test_string = "".to_owned();
    for _ in 0..1_000_000 {
        test_string +=  &format!("addition");
    }

    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```
211.14ms
```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut vec_string: Vec<u8> = vec!();
    vec_string.reserve(8_000_000);
    for _ in 0..1_000_000 {
        for byte in "addition".as_bytes().iter() {
            vec_string.push(*byte);
        }
    }
    let test_string = String::from_utf8(vec_string).unwrap();
    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```

```rust
fn main() {
    use std::time::Instant;
    let now = Instant::now();

    let mut vec_string: Vec<u8> = vec!();
    for _ in 0..1_000_000 {
        for byte in "addition".as_bytes().iter() {
            vec_string.push(*byte);
        }
    }
    let test_string = String::from_utf8(vec_string).unwrap();
    
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}
```