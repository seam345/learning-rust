# Summary

My learning in rust in no real order:

### I have things with lots of code examples in this book:

- [String building](./chapter_1.md)

### And more general blog posts about rust on my website
- 


##  open source projects I watch/use
### Cool crates I have my eye on/use
#### testing
- https://crates.io/crates/insta fast snapshot testing
- https://crates.io/crates/inquire/0.5.3 simple command line prompt helper

### Helpful developer tools I use
- https://crates.io/crates/typos find spelling mistakes in source code


## other blog that have effected how i code

### [The Importance of Logging](https://www.thecodedmessage.com/posts/logging/)
great article on a way to think about when to make a log statement
